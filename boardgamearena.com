// ==UserScript==
// @name     Remove online/offline 850209
// @version  1
// @match    https://boardgamearena.com/*
// @grant    none
// ==/UserScript==

(async function() {
    'use strict';

    function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    while (true)
    {
        console.log('removing logs...');
        Array.from(document.getElementsByClassName('log')).filter(t => t.innerText.match(' (jest|is now) ')).forEach(e => e.remove());
        await sleep(60000);
    }
})();
