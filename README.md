# Greasemonkey Scripts

My collection of Greasemonkey scripts I use.

# Development

Some documentation can be found here: https://wiki.greasespot.net/GM.xmlHttpRequest

## Timeout

Running some code with a delay can be achieved by using this pattern:

```
    const runSomeCode = () => {
        return setTimeout(runSomeCode, 10000);
    };
 
    runSomeCode();
```
